package com.task.turbo.task.turbo.dto;

import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private String product_carName;
    private String product_image;
    private String product_renewalDate;
    private Long product_viewCount;
    private Double product_price;
    private String product_description;
    private String product_city;
    private String product_brand;
    private String product_model;
    private Long product_yearOfManufacture;
    private String product_bodyType;
    private String  product_color;
    private String product_engine;
    private String product_mileage;
    private String  product_transmission;
    private String product_drivetrain;
    private String product_isNew;
    private Integer product_seatingCapacity;
    private String product_owners;
    private String product_status;
    private String product_forWitchMarket;
}
