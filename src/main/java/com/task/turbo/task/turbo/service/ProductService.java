package com.task.turbo.task.turbo.service;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.ProductPageDto;
import com.task.turbo.task.turbo.dto.ProductRequestDto;
import com.task.turbo.task.turbo.dto.ProductSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    ProductDto createProduct(ProductRequestDto productRequestDto);

    ProductDto updateProduct(Long id, ProductRequestDto productRequestDto);

    ProductDto getProductById(Long id);

    List<ProductDto> getProductListWithUserId(Long id);

    void deleteProductById(Long id);


    Page<ProductPageDto> getProductList(Pageable pageable);

    List<ProductDto> searchProduct(ProductSearchDto productSearchDto);

}
