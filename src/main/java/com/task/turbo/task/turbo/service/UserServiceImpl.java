package com.task.turbo.task.turbo.service;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.UserDto;
import com.task.turbo.task.turbo.dto.UserRequestDto;
import com.task.turbo.task.turbo.entity.ProductEntity;
import com.task.turbo.task.turbo.entity.UserEntity;
import com.task.turbo.task.turbo.exception.NotFoundException;
import com.task.turbo.task.turbo.repository.ProductRepository;
import com.task.turbo.task.turbo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    @Override
    public UserDto createUser(UserRequestDto userRequestDto) {
        UserEntity userEntity = UserEntity.builder()
                .user_Name(userRequestDto.getUser_Name())
                .user_PhoneNumber(userRequestDto.getUser_PhoneNumber())
                .user_description(userRequestDto.getUser_description())
                .user_announcementCount(userRequestDto.getUser_announcementCount())
                .user_image(userRequestDto.getUser_image())
                .build();
        final UserEntity save = userRepository.save(userEntity);
        return UserDto.builder()
                .id(save.getId())
                .user_Name(save.getUser_Name())
                .user_PhoneNumber(save.getUser_PhoneNumber())
                .user_description(save.getUser_description())
                .user_announcementCount(save.getUser_announcementCount())
                .user_image(save.getUser_image())
                .build();
    }


    @Override
    public UserDto updateUser(Long id, UserRequestDto userRequestDto) {
        UserEntity userEntity = UserEntity.builder()
                .id(id)
                .user_Name(userRequestDto.getUser_Name())
                .user_PhoneNumber(userRequestDto.getUser_PhoneNumber())
                .user_description(userRequestDto.getUser_description())
                .user_announcementCount(userRequestDto.getUser_announcementCount())
                .user_image(userRequestDto.getUser_image())
                .build();
        final UserEntity save = userRepository.save(userEntity);
        return UserDto.builder()
                .id(save.getId())
                .user_Name(save.getUser_Name())
                .user_PhoneNumber(save.getUser_PhoneNumber())
                .user_description(save.getUser_description())
                .user_announcementCount(save.getUser_announcementCount())
                .user_image(save.getUser_image())
                .build();
    }

    @Override
    public UserDto getUserById(Long id) {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new NotFoundException("No user found with ID " + id));
        final List<ProductDto> productDto = userEntity.getProductEntities().stream().map(productEntity -> modelMapper.map(productEntity, ProductDto.class))
                .collect(Collectors.toList());
        return UserDto.builder()
                .id(userEntity.getId())
                .user_Name(userEntity.getUser_Name())
                .user_PhoneNumber(userEntity.getUser_PhoneNumber())
                .user_description(userEntity.getUser_description())
                .user_announcementCount(userEntity.getUser_announcementCount())
                .user_image(userEntity.getUser_image())
                .productDtoList(productDto)
                .build();
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserDto updateUserWithProduct(Long userId, Long productId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("No user found with ID " + userId));
        ProductEntity product = productRepository.findById(productId).orElseThrow(() -> new NotFoundException("No product found with ID " + productId));
        List<ProductEntity> productList = userEntity.getProductEntities();
        productList.add(product);
        userEntity.setProductEntities(productList);
        UserEntity updatedUserEntity = userRepository.save(userEntity);
        UserDto userDto = modelMapper.map(updatedUserEntity, UserDto.class);
        ProductDto productDto = modelMapper.map(product, ProductDto.class);
        userDto.setProductDtoList(Collections.singletonList(productDto));
        return userDto;
    }

    @Override
    public Page<UserDto> listUser(Pageable pageable) {
         return userRepository.findAll(pageable)
                .map(userEntity -> modelMapper.map(userEntity,UserDto.class));
    }

}
