package com.task.turbo.task.turbo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductPageDto {
    private Double price;
    private String name;
    private String city;
    private String image;
}
