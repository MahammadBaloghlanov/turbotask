package com.task.turbo.task.turbo.rest;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.UserDto;
import com.task.turbo.task.turbo.dto.UserRequestDto;
import com.task.turbo.task.turbo.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;

    @PostMapping
    public UserDto create(@RequestBody UserRequestDto userRequestDto){
        return userService.createUser(userRequestDto);
    }

    @PutMapping("/{id}")
    public UserDto update(@PathVariable Long id , @RequestBody UserRequestDto userRequestDto){
        return userService.updateUser(id,userRequestDto);
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        userService.deleteUser(id);
    }

    @PutMapping("/{userId}/{productId}")
    public UserDto updateUserWithProduct(@PathVariable  Long userId, @PathVariable Long productId){
        return userService.updateUserWithProduct(userId,productId);
    }

    @GetMapping
    public Page<UserDto> getList(Pageable pageable){
        log.info("Received page {} size{}",pageable);
        return userService.listUser(pageable);
    }


}
