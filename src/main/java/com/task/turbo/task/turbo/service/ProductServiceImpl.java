package com.task.turbo.task.turbo.service;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.ProductPageDto;
import com.task.turbo.task.turbo.dto.ProductRequestDto;
import com.task.turbo.task.turbo.dto.ProductSearchDto;
import com.task.turbo.task.turbo.entity.ProductEntity;
import com.task.turbo.task.turbo.entity.UserEntity;
import com.task.turbo.task.turbo.exception.NotFoundException;
import com.task.turbo.task.turbo.repository.ProductRepository;
import com.task.turbo.task.turbo.repository.UserRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    private final UserRepository userRepository;
    @Override
    public ProductDto createProduct(ProductRequestDto productRequestDto) {
        ProductEntity productEntity = ProductEntity.builder()
                .product_carName(productRequestDto.getProduct_carName())
                .product_image(productRequestDto.getProduct_image())
                .product_renewalDate(productRequestDto.getProduct_renewalDate())
                .product_viewCount(productRequestDto.getProduct_viewCount())
                .product_city(productRequestDto.getProduct_city())
                .product_brand(productRequestDto.getProduct_brand())
                .product_model(productRequestDto.getProduct_model())
                .product_yearOfManufacture(productRequestDto.getProduct_yearOfManufacture())
                .product_bodyType(productRequestDto.getProduct_bodyType())
                .product_color(productRequestDto.getProduct_color())
                .product_engine(productRequestDto.getProduct_engine())
                .product_mileage(productRequestDto.getProduct_mileage())
                .product_transmission(productRequestDto.getProduct_transmission())
                .product_drivetrain(productRequestDto.getProduct_drivetrain())
                .product_isNew(productRequestDto.getProduct_isNew())
                .product_seatingCapacity(productRequestDto.getProduct_seatingCapacity())
                .product_owners(productRequestDto.getProduct_owners())
                .product_status(productRequestDto.getProduct_status())
                .product_forWitchMarket(productRequestDto.getProduct_forWitchMarket())
                .product_price(productRequestDto.getProduct_price())
                .product_description(productRequestDto.getProduct_description())
                .build();
        final ProductEntity save = productRepository.save(productEntity);
        return ProductDto.builder()
                .id(save.getId())
                .product_carName(save.getProduct_carName())
                .product_image(save.getProduct_image())
                .product_renewalDate(save.getProduct_renewalDate())
                .product_viewCount(save.getProduct_viewCount())
                .product_city(save.getProduct_city())
                .product_brand(save.getProduct_brand())
                .product_model(save.getProduct_model())
                .product_yearOfManufacture(save.getProduct_yearOfManufacture())
                .product_bodyType(save.getProduct_bodyType())
                .product_color(save.getProduct_color())
                .product_engine(save.getProduct_engine())
                .product_mileage(save.getProduct_mileage())
                .product_transmission(save.getProduct_transmission())
                .product_drivetrain(save.getProduct_drivetrain())
                .product_isNew(save.getProduct_isNew())
                .product_seatingCapacity(save.getProduct_seatingCapacity())
                .product_owners(save.getProduct_owners())
                .product_status(save.getProduct_status())
                .product_forWitchMarket(save.getProduct_forWitchMarket())
                .product_price(save.getProduct_price())
                .product_description(save.getProduct_description())
                .build();
    }

    @Override
    public ProductDto updateProduct(Long id, ProductRequestDto productRequestDto) {
        ProductEntity productEntity = ProductEntity.builder()
                .id(id)
                .product_carName(productRequestDto.getProduct_carName())
                .product_image(productRequestDto.getProduct_image())
                .product_renewalDate(productRequestDto.getProduct_renewalDate())
                .product_viewCount(productRequestDto.getProduct_viewCount())
                .product_city(productRequestDto.getProduct_city())
                .product_brand(productRequestDto.getProduct_brand())
                .product_model(productRequestDto.getProduct_model())
                .product_yearOfManufacture(productRequestDto.getProduct_yearOfManufacture())
                .product_bodyType(productRequestDto.getProduct_bodyType())
                .product_color(productRequestDto.getProduct_color())
                .product_engine(productRequestDto.getProduct_engine())
                .product_mileage(productRequestDto.getProduct_mileage())
                .product_transmission(productRequestDto.getProduct_transmission())
                .product_drivetrain(productRequestDto.getProduct_drivetrain())
                .product_isNew(productRequestDto.getProduct_isNew())
                .product_seatingCapacity(productRequestDto.getProduct_seatingCapacity())
                .product_owners(productRequestDto.getProduct_owners())
                .product_status(productRequestDto.getProduct_status())
                .product_forWitchMarket(productRequestDto.getProduct_forWitchMarket())
                .product_price(productRequestDto.getProduct_price())
                .product_description(productRequestDto.getProduct_description())
                .build();
        final ProductEntity save = productRepository.save(productEntity);
        return ProductDto.builder()
                .id(save.getId())
                .product_carName(save.getProduct_carName())
                .product_image(save.getProduct_image())
                .product_renewalDate(save.getProduct_renewalDate())
                .product_viewCount(save.getProduct_viewCount())
                .product_city(save.getProduct_city())
                .product_brand(save.getProduct_brand())
                .product_model(save.getProduct_model())
                .product_yearOfManufacture(save.getProduct_yearOfManufacture())
                .product_bodyType(save.getProduct_bodyType())
                .product_color(save.getProduct_color())
                .product_engine(save.getProduct_engine())
                .product_mileage(save.getProduct_mileage())
                .product_transmission(save.getProduct_transmission())
                .product_drivetrain(save.getProduct_drivetrain())
                .product_isNew(save.getProduct_isNew())
                .product_seatingCapacity(save.getProduct_seatingCapacity())
                .product_owners(save.getProduct_owners())
                .product_status(save.getProduct_status())
                .product_forWitchMarket(save.getProduct_forWitchMarket())
                .product_price(save.getProduct_price())
                .product_description(save.getProduct_description())
                .build();
    }

    @Override
    public ProductDto getProductById(Long id) {
        final ProductEntity productEntity = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No product found with ID " + id));
        productEntity.setProduct_viewCount(productEntity.getProduct_viewCount()+1);
        productRepository.save(productEntity);
        return ProductDto.builder()
                .id(productEntity.getId())
                .product_carName(productEntity.getProduct_carName())
                .product_image(productEntity.getProduct_image())
                .product_renewalDate(productEntity.getProduct_renewalDate())
                .product_viewCount(productEntity.getProduct_viewCount())
                .product_city(productEntity.getProduct_city())
                .product_brand(productEntity.getProduct_brand())
                .product_model(productEntity.getProduct_model())
                .product_yearOfManufacture(productEntity.getProduct_yearOfManufacture())
                .product_bodyType(productEntity.getProduct_bodyType())
                .product_color(productEntity.getProduct_color())
                .product_engine(productEntity.getProduct_engine())
                .product_mileage(productEntity.getProduct_mileage())
                .product_transmission(productEntity.getProduct_transmission())
                .product_drivetrain(productEntity.getProduct_drivetrain())
                .product_isNew(productEntity.getProduct_isNew())
                .product_seatingCapacity(productEntity.getProduct_seatingCapacity())
                .product_owners(productEntity.getProduct_owners())
                .product_status(productEntity.getProduct_status())
                .product_forWitchMarket(productEntity.getProduct_forWitchMarket())
                .product_price(productEntity.getProduct_price())
                .product_description(productEntity.getProduct_description())
                .build();
    }
    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<ProductPageDto> getProductList(Pageable pageable) {
        final Page<ProductEntity> productEntities = productRepository.findAll(pageable);
        Page<ProductPageDto> productPageDto=productEntities
                .map(productEntity->ProductPageDto.builder()
                        .price(productEntity.getProduct_price())
                        .name(productEntity.getProduct_carName())
                        .city(productEntity.getProduct_city())
                        .image(productEntity.getProduct_image())
                        .build());
        return productPageDto;
    }

    @Override
    public List<ProductDto> searchProduct(ProductSearchDto productSearchDto) {
        List<ProductEntity> all = productRepository.findAll((Specification<ProductEntity>) (root, query, criteriaBuilder) -> {
            Predicate[] specification = specification(productSearchDto, root, criteriaBuilder);
            return criteriaBuilder.and(specification);
        });
        List<ProductDto> productDtoList = all.stream()
                .map(productEntity -> modelMapper.map(productEntity, ProductDto.class))
                .collect(Collectors.toList());

        return productDtoList;
    }


    @Override
    public List<ProductDto> getProductListWithUserId(Long id) {
        final UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new NotFoundException("No user found with ID " + id));
        final List<ProductDto> productDto = userEntity.getProductEntities().stream().map(productEntity -> modelMapper.map(productEntity, ProductDto.class))
                .collect(Collectors.toList());
        return productDto;
    }

    private Predicate[] specification(ProductSearchDto productSearchDto ,
                                      Root<ProductEntity> root,
                                      CriteriaBuilder criteriaBuilder){
        List<Predicate> predicateList = new ArrayList<>();
        if (productSearchDto.getBrand() != null){
            final Predicate brand = criteriaBuilder.equal(root.get(ProductEntity.Fields.product_brand), productSearchDto.getBrand());
            predicateList.add(brand);
        }
        if (productSearchDto.getModel() != null){
            final Predicate model = criteriaBuilder.equal(root.get(ProductEntity.Fields.product_model), productSearchDto.getModel());
            predicateList.add(model);
        }
        if (productSearchDto.getMinimumPrice() != null || productSearchDto.getMaximumPrice() != null){
            if (productSearchDto.getMinimumPrice() != null && productSearchDto.getMaximumPrice() != null) {
                final Predicate price = criteriaBuilder.between(root.get(ProductEntity.Fields.product_price),
                        productSearchDto.getMinimumPrice(),
                        productSearchDto.getMaximumPrice());
                predicateList.add(price);
            } else if (productSearchDto.getMinimumPrice() != null) {
                final Predicate price = criteriaBuilder.greaterThanOrEqualTo(root.get(ProductEntity.Fields.product_price)
                        , productSearchDto.getMinimumPrice());
                predicateList.add(price);
            }else {
                final Predicate price = criteriaBuilder.lessThanOrEqualTo(root.get(ProductEntity.Fields.product_price)
                ,productSearchDto.getMaximumPrice());
                predicateList.add(price);
            }
        }
        if (productSearchDto.getYearOfManufacture() != null){
            final Predicate year = criteriaBuilder.greaterThanOrEqualTo(root.get(ProductEntity.Fields.product_yearOfManufacture),
                    productSearchDto.getYearOfManufacture());
            predicateList.add(year);
        }

        return predicateList.toArray(new Predicate[0]);

    }
}
