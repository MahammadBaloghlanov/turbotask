package com.task.turbo.task.turbo.repository;

import com.task.turbo.task.turbo.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity,Long> {
}
