package com.task.turbo.task.turbo.service;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.UserDto;
import com.task.turbo.task.turbo.dto.UserRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    UserDto createUser(UserRequestDto userRequestDto);

    UserDto updateUser(Long id, UserRequestDto userRequestDto);

    UserDto getUserById(Long id);

    void deleteUser(Long id);


    UserDto updateUserWithProduct(Long userId, Long productId);

    Page<UserDto> listUser(Pageable pageable);



}
