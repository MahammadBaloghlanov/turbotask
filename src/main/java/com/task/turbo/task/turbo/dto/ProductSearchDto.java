package com.task.turbo.task.turbo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductSearchDto {
    private String model;
    private String brand;
    private Double price;
    private Double minimumPrice;
    private Double maximumPrice;
    private Long yearOfManufacture;

}
