package com.task.turbo.task.turbo.rest;

import com.task.turbo.task.turbo.dto.ProductDto;
import com.task.turbo.task.turbo.dto.ProductPageDto;
import com.task.turbo.task.turbo.dto.ProductRequestDto;
import com.task.turbo.task.turbo.dto.ProductSearchDto;
import com.task.turbo.task.turbo.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping
    public ProductDto create(@RequestBody ProductRequestDto productRequestDto){
        return productService.createProduct(productRequestDto);
    }

    @PutMapping("/{id}")
    public ProductDto update(@PathVariable Long id,@RequestBody ProductRequestDto productRequestDto){
        return productService.updateProduct(id,productRequestDto);
    }

    @GetMapping("/{id}")
    public ProductDto get(@PathVariable Long id){
        return productService.getProductById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        productService.deleteProductById(id);
    }

    @GetMapping("/user/{id1}")
    public List<ProductDto> getProductWithUserId(@PathVariable Long id1){
        return productService.getProductListWithUserId(id1);
    }

//    @GetMapping
//    public Page<ProductPageDto> productList(Pageable pageable){
//        return productService.getProductList(pageable);
//    }

    @GetMapping
    public List<ProductDto> productList(@RequestBody ProductSearchDto productSearchDto){
        return productService.searchProduct(productSearchDto);
    }

}
