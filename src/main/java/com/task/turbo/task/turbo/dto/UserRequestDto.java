package com.task.turbo.task.turbo.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserRequestDto {
    private String user_Name;
    private String user_PhoneNumber;
    private String user_description;
    private String user_image;
    private Integer user_announcementCount;
    private List<ProductDto> productDtoList;
}
