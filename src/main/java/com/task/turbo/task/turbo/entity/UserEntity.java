package com.task.turbo.task.turbo.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Entity
@Table(name = "User")
@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldNameConstants
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String user_Name;
    private String user_PhoneNumber;
    private String user_description;
    private String user_image;
    private Integer user_announcementCount;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<ProductEntity> productEntities;
}
