package com.task.turbo.task.turbo.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String user_Name;
    private String user_PhoneNumber;
    private String user_description;
    private String user_image;
    private Integer user_announcementCount;
    private List<ProductDto> productDtoList;
}
